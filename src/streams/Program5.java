package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Program5 {
    public static <list> void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("b");
        list.add("e");
        list.add("d");
        list.add("k");
        System.out.println(list);
        List<String> elements =
                Stream.of("a", "b", "c").filter(element -> element.contains("a"))
                        .collect(Collectors.toList());
        Optional<String> anyElement = elements.stream().findAny();
//        Optional<String> firstElement = elements.stream().findFirst();
        System.out.println(anyElement);
//        System.out.println(firstElement);

    }
}
