package streams;

import java.util.Arrays;
import java.util.List;

public class Program8 {
    public static void main(String[] args) {
        List<Integer> list= Arrays.asList(12,11,9,1,20,62);
        list.stream().map(Integer->Integer+"").filter(Integer->Integer.startsWith("1")).forEach(System.out::println);
    }
    }
