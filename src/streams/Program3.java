package streams;

import java.util.HashSet;
import java.util.OptionalInt;
import java.util.Set;

public class Program3 {
    public static void main(String[] args) {
        String name = "madma";
        /*Stream<Object> str = name.chars().mapToObj(s -> Character.toLowerCase(Character.valueOf((char) s))).collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()))
                .entrySet().stream().filter(entry -> entry.getValue() > 1L).map(entry -> entry.getKey());
        System.out.println(str);*/
        /*Map< Character, Long > result = name
                .chars().mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));

        result.forEach((k, v) -> {
            if (v > 1) {
                System.out.println(k + " : " + v);
            }
        });*/

        Set<Integer> seen = new HashSet<>();
        OptionalInt first = name.chars()
                .filter(i -> !seen.add(i))
                .findFirst();
        if (first.isPresent()) {
            System.out.println((char) first.getAsInt());
        }
    }
}
