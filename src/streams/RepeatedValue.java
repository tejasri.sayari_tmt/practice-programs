package streams;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RepeatedValue {
    public static void main(String[] args) {
        String name="Hello";
        Character result=name.chars().mapToObj(String->Character.toLowerCase(Character.valueOf((char)String)))
                .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new,Collectors.counting()))
                .entrySet().stream().filter(entry->entry.getValue()>1L).map(Map.Entry::getKey).findFirst().get();
        System.out.println(result);
    }
}

