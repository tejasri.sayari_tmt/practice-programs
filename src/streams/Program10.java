package streams;

import java.util.Arrays;
import java.util.List;

public class Program10 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(12, 11, 9, 1, 20, 62, 9, 12);
        list.stream().findFirst().ifPresent(System.out::println);
    }
}

