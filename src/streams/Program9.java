package streams;


import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Program9 {
    public static void main(String[] args) {
        List<Integer> list= Arrays.asList(12,11,9,1,20,62,9,12);
        Set<Integer> set=new HashSet();
        list.stream().filter(Integer->!set.add(Integer)).forEach(System.out::println);

    }
}
