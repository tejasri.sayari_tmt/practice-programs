package streams;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DescendingOrder {
    public static void main(String[] args) {
        List<Integer> list= Arrays.asList(2,8,4,8,9,1,5,10);
        list.stream().sorted(Collections.reverseOrder()).forEach(System.out::println);

    }
}
