package streams;

import java.util.Arrays;
import java.util.List;

public class Sort {
    public static void main(String[] args) {
        List<Integer> list= Arrays.asList(8,6,5,4,9,2,1,8);
        list.stream().sorted().forEach(System.out::println);
    }
}
