package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Program12 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(12, 11, 9, 20, 62, 9, 12);
        Optional<Integer> max= list.stream().max(Integer::compare);
        System.out.println(max);

    }
}
